#!/usr/bin/env bash
TARGET="pleroma@froth.zone:/opt/pleroma"

rsync --update -Pr dist/ "${TARGET}/instance/static/frontends/admin-fe/froth"

